import 'package:article/providers/articleview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:provider/provider.dart';

import 'articleview/aticlepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      builder: (context,child){
        return Directionality(textDirection: TextDirection.rtl, child: child);
      },
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 52, 61, 70),
        body: FutureBuilder(
          future: DefaultCacheManager()
              .getSingleFile('http://www.realestatedealer.ir/aa.txt'),
          builder: (context, snap) {
            if (snap.hasData) {
              var file = snap.data;
              return FutureBuilder(
                future: file.readAsString(),
                builder: (context, snap) {
                  if (snap.hasData) {
                    return ChangeNotifierProvider.value(
                      value: ProviderArticle(context),
                      child: ArticlePage(
                          article: snap.data,
                          screenWidth: MediaQuery.of(context).size.width),
                    );
                  }
                  return Center(child: CircularProgressIndicator());
                },
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}

