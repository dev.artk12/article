import 'package:flutter/material.dart';

class MyColor {
  static final Color buttonBackgroundColor = Color.fromARGB(255, 56, 56, 56);
  static final Color buttonTextColor = Color.fromARGB(255, 224, 224, 224);
  static final Color textColor = Color.fromARGB(255, 52, 61, 70);
  static final Color signColorBackgroundColor = Color.fromARGB(220, 84, 84, 84);
  static final List<Color> noteColors = [Colors.red, Colors.blue, Colors.green];
  static final List<String> myNoteText = ['مهم', 'عادی', 'کم'];
  static final List<Color> lightTextColors = [
    Color.fromARGB(255, 16, 43, 122),
    Color.fromARGB(255, 122, 16, 53),
    Color.fromARGB(255, 16, 92, 122),
    Color.fromARGB(255, 52,61,70),
  ];
  static final List<Color> darkTextColors = [
    Color.fromARGB(255, 255, 82, 82),
    Colors.white,
    Colors.cyan,
    Colors.tealAccent,
    Colors.yellow,
    Colors.lightBlueAccent,
    Colors.lightGreen,
  ];


  static List<MyArticleColor> myArticleColor() {
    List<MyArticleColor> myArticleColors = [];
    for(int i = 0 ; i < lightTextColors.length; i++){
      myArticleColors.add(MyArticleColor(backgroundColor: Colors.white,textColor: lightTextColors[i]));
    }
    for(int i = 0 ; i < darkTextColors.length; i++){
      myArticleColors.add(MyArticleColor(backgroundColor: Color(0xFFB1F2B36),textColor: darkTextColors[i]));
    }
    return myArticleColors;
  }
}

class MyArticleColor {
  Color textColor;
  Color backgroundColor;
  MyArticleColor({this.backgroundColor, this.textColor});
}
