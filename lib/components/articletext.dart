import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ArticleText extends StatelessWidget {
  final String text;
  final Function onTap;
  final double size;
  final String font;
  final Color color;
  ArticleText({this.text, this.onTap,this.size,this.font = 'iranian_sans',this.color});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text("$text",
          style:
              TextStyle(fontSize: size,fontFamily: font, color:color,)),
    );
  }
}
//light
//Color.fromARGB(255, 16, 43, 122)
//Color.fromARGB(255, 122, 16, 53)
//Color.fromARGB(255, 16, 92, 122)
//Color.fromARGB(255, 52,61,70)

//dark
//Color.fromARGB(255, 255, 82, 82)
//lightGreen //lightBlue //yellow //tealAccent // cyan //white