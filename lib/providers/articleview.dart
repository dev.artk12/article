import 'package:article/colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProviderArticle extends ChangeNotifier {
  Color articleBackground;
  Color articleForeground;
  bool sideBarOpen = false;
  double xOffset = 0;
  double yOffset = 0;
  double pageScale = 1;
  double screenWidth;
  int fontVal = 0;
  double fontSize = 16;
  String fontLabelText = '16';
  int myArticleColorIndex = 0;
  MyArticleColor myArticleColor =
      MyArticleColor(backgroundColor: Color(0xFFB1F2B36), textColor: Colors.white);


  void onChangeMyArticleColor(MyArticleColor myArticleColor,int index){
    this.myArticleColorIndex = index;
    this.myArticleColor = myArticleColor;
    notifyListeners();
  }

  ProviderArticle(BuildContext context) {
    this.screenWidth = MediaQuery.of(context).size.width;
    xOffset = sideBarOpen ? screenWidth / 2 - 5 : 0;
    yOffset = sideBarOpen ? 10 : 0;
    pageScale = sideBarOpen ? 0.5 : 1;
  }

  void updateFontVal(int i) {
    this.fontVal = i;
    notifyListeners();
  }

  onChangeFontSize(double size) {
    this.fontSize = size;
    if (size == 14.0) {
      fontLabelText = '14';
    } else if (size == 16.0) {
      fontLabelText = '16';
    } else if (size == 18.0) {
      fontLabelText = '18';
    }
    notifyListeners();
  }

  void changeArticleBackgroundPage(Color color) {
    this.articleBackground = color;
    notifyListeners();
  }

  void changeArticleForeGroundPage(Color color) {
    this.articleForeground = color;
    notifyListeners();
  }

  void setSideBarState() {
    sideBarOpen = !sideBarOpen;
    xOffset = sideBarOpen ? screenWidth / 2 - 5 : 0;
    yOffset = sideBarOpen ? 10 : 0;
    pageScale = sideBarOpen ? 0.5 : 1;
    notifyListeners();
  }
}
