import 'package:article/articleview/background.dart';
import 'package:article/articleview/foreground.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:article/colors/colors.dart';
import 'package:article/providers/articleview.dart';
import 'package:provider/provider.dart';

class ArticlePage extends StatefulWidget {
  createState() => ArticlePageState();
  final double screenWidth;
  final String article;
  ArticlePage({this.screenWidth, this.article});
}

class ArticlePageState extends State<ArticlePage>
    with TickerProviderStateMixin {

  // final ProviderArticle providerArticle;

  ScrollController _controller;
  int check = 3;
  double lastPos = 0;
  double cWidth = 0;
  bool isUserScrolling = false;
  bool autoScrollAgain = true;
  int x;
  AnimationController _animController;
  AnimationController _fadeAnimController;
  Animation<Offset> _offsetAnimation;
  Animation<double> _fadeInFadeOut;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _animController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
    _fadeAnimController = AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    );
    // _fadeInFadeOut = Tween<double>(begin: 0.0, end: 0.5).animate(_fadeAnimController);
    _openContainer();
    _controller.addListener(onScroll);
  }

  void onScroll() {
    x = ((_controller.offset / _controller.position.maxScrollExtent) * 100)
        .round();
    setState(() {
      cWidth = ((widget.screenWidth / 100) * x);
      autoScrollAgain = false;
    });
  }

  void _fadeIn() {
    _fadeInFadeOut =
        Tween<double>(begin: 0.0, end: 1.0).animate(_fadeAnimController);
    _fadeAnimController.reset();
    _fadeAnimController.forward();
  }

  void _fadeOut() {
    _fadeInFadeOut =
        Tween<double>(begin: 1.0, end: 0.0).animate(_fadeAnimController);
    _fadeAnimController.reset();
    _fadeAnimController.forward();
  }

  _scroll() async {
    if (check == 1) {
      while (true) {
        if (_controller.position.pixels >=
                _controller.position.maxScrollExtent ||
            check != 1) {
          lastPos = _controller.position.pixels + 0.2;
          break;
        }
        await Future.delayed(Duration(milliseconds: 20));
        if (lastPos != 0) {
          lastPos = 0;
        }
        _controller.animateTo(_controller.position.pixels + 0.15,
            duration: Duration(milliseconds: 20), curve: Curves.linear);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _animController.dispose();
    _fadeAnimController.dispose();
  }

  _openContainer() {
    _offsetAnimation = new Tween<Offset>(
      begin: const Offset(-1.5, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(_animController);
    _animController.reset();
    _animController.forward();
    _fadeIn();
  }

  _closeContainer() {
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(-1.5, 0.0),
    ).animate(_animController);
    _animController.reset();
    _animController.forward();
    _fadeOut();
  }

  @override
  Widget build(BuildContext context) {
    ProviderArticle providerArticle = Provider.of<ProviderArticle>(context);

    if (autoScrollAgain) {
      _scroll();
    }

    return Scaffold(
      backgroundColor: MyColor.textColor,
      body: SafeArea(
        child: Center(
          child: NotificationListener(
            onNotification: (ScrollNotification notification) {
              if (lastPos != 0) {
                if (check != 1) {
                  setState(() {
                    autoScrollAgain = true;
                    isUserScrolling = false;
                  });
                }
              }
              return true;
            },
            child: Stack(
              children: [
                ArticleBackground(
                  providerArticle: providerArticle,
                ),
                AnimatedContainer(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 250),
                  decoration: BoxDecoration(
                    // color: Colors.white,
                    color: providerArticle.myArticleColor.backgroundColor,
                    borderRadius: providerArticle.sideBarOpen
                        ? BorderRadius.circular(20)
                        : BorderRadius.circular(0),
                    // border: Border.all(color: Colors.white, width: 1)
                    // border: providerArticle.sideBarOpen
                    //     ? Border.all(color: Colors.white, width: 1)
                    //     : Border.all(color: Colors.transparent, width: 1),
                  ),
                  transform: Matrix4.translationValues(
                      providerArticle.xOffset, providerArticle.yOffset, 1)
                    ..scale(providerArticle.pageScale),
                  child: GestureDetector(

                    onHorizontalDragEnd: (DragEndDetails dragEnd) {
                      // print(isUserScrolling);
                      // print(check);
                      // print(autoScrollAgain);
                      providerArticle.setSideBarState();
                      if (providerArticle.sideBarOpen) {
                        if (!isUserScrolling) {
                          setState(() {
                            isUserScrolling = false;
                            autoScrollAgain = true;
                            check = 1;
                            _closeContainer();
                          });
                        }else{
                          setState(() {
                            isUserScrolling = true;
                            autoScrollAgain = false;
                            check = 2;
                            _openContainer();
                          });
                        }
                      } else if (!providerArticle.sideBarOpen) {
                        if(isUserScrolling){
                          setState(() {
                            isUserScrolling = false;
                            autoScrollAgain = false;
                            check = 2;
                            _openContainer();
                          });
                        }
                      } else {
                        if (!autoScrollAgain && isUserScrolling) {

                          setState(() {
                            isUserScrolling = false;
                            autoScrollAgain = false;
                            check = 2;
                            _openContainer();
                          });
                        }
                      }
                      // else{
                      //     setState(() {
                      //       // isUserScrolling = false;
                      //       // autoScrollAgain = true;
                      //       check = 4;
                      //       _openContainer();
                      //     });
                      //   }
                      // }
                    },
                    onVerticalDragUpdate: (DragUpdateDetails dragUpdate) {},
                    onTapDown: (_) {
                      if (check == 4) {}
                      if (check == 3) {
                        setState(() {
                          isUserScrolling = false;
                          autoScrollAgain = false;
                          check = 2;
                          _openContainer();
                        });
                      } else {
                        if (check == 1) {
                          setState(() {
                            isUserScrolling = true;
                            check = 2;
                            _openContainer();
                          });
                        }
                      }
                    },
                    onTap: () {
                      if (check == 3) {
                        setState(() {
                          isUserScrolling = false;
                          autoScrollAgain = true;
                          check = 1;
                          _closeContainer();
                        });
                      } else {
                        if (providerArticle.sideBarOpen) {
                          providerArticle.setSideBarState();
                          setState(() {
                            isUserScrolling = false;
                            autoScrollAgain = true;
                            check = 2;
                          });
                        } else {
                          if (check == 2 && !isUserScrolling) {
                            setState(() {
                              autoScrollAgain = true;
                              check = 1;
                              _closeContainer();
                            });
                          } else {
                            if (isUserScrolling) {
                              setState(() {
                                isUserScrolling = false;
                                autoScrollAgain = false;
                                check = 2;
                                _openContainer();
                              });
                            }
                          }
                        }
                      }
                    },
                    child: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            boxShadow: [
                              BoxShadow(color: Colors.blue,blurRadius: 15),
                            ]
                          ),
                          margin: providerArticle.sideBarOpen
                              ? EdgeInsets.only(left: 10, right: 10)
                              : null,
                          width: cWidth + 0.0,
                          height: 3,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ForeGround(
                            providerArticle: providerArticle,
                            controller: _controller,
                            text: widget.article,
                          ),
                        ),
                        isUserScrolling
                            ? Container()
                            : providerArticle.sideBarOpen
                                ? Container()
                                : FadeTransition(
                                    opacity: _fadeInFadeOut,
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: SlideTransition(
                                        position: _offsetAnimation,
                                        child: InkWell(
                                          onTap: () {
                                            providerArticle.setSideBarState();
                                          },
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(20),
                                              bottomRight: Radius.circular(20),
                                            ),
                                            child: Container(
                                              color: Colors.blue[300],
                                              height: 200,
                                              width: 25,
                                              child: Icon(
                                                Icons.arrow_back_ios,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
