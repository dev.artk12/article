import 'package:article/colors/colors.dart';
import 'package:article/colors/font.dart';
import 'package:article/components/articletext.dart';
import 'package:article/components/simpletext.dart';
import 'package:article/providers/articleview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ArticleBackground extends StatelessWidget {
  final ProviderArticle providerArticle;
  List<MyArticleColor> myArticlesColor = MyColor.myArticleColor();

  ArticleBackground({this.providerArticle});

  @override
  Widget build(BuildContext context) {
    List<DropdownListItem> fontList = MyFont.getListDropDown();
    return Container(
      color: Color.fromARGB(255,36, 35, 43),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.only(left: 5, top: 7, right: 5),
              width: MediaQuery.of(context).size.width / 2 - 10,
              height: MediaQuery.of(context).size.height / 2,
              // color: Colors.white,
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    color: providerArticle.myArticleColor.backgroundColor,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 1, color: Colors.white),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ArticleText(
                      text: 'این متن نمونه است ...',
                      color: providerArticle.myArticleColor.textColor,
                      size: providerArticle.fontSize,
                      font: fontList[providerArticle.fontVal].name,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(left: 9, top: 9, right: 9),
              // width: MediaQuery.of(context).size.width/2-10,
              height: MediaQuery.of(context).size.height / 2 - 30,
              // color: Colors.blue,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child:
                            Center(child: SimpleText(text: 'اندازه فونت :')),
                      ),
                      Expanded(
                        flex: 3,
                        child: SliderTheme(
                          data: SliderThemeData(
                              valueIndicatorColor: Colors
                                  .blue, // This is what you are asking for
                              activeTrackColor: Colors.white,
                              overlayColor: Color(
                                  0x29EB1555), // Custom Thumb overlay Color
                              thumbShape: RoundSliderThumbShape(
                                  enabledThumbRadius: 12.0),
                              overlayShape: RoundSliderOverlayShape(
                                  overlayRadius: 20.0),
                              trackHeight: 12),
                          child: Slider(
                              value: providerArticle.fontSize,
                              min: 14.0,
                              max: 18.0,
                              divisions: 2,
                              label: providerArticle.fontLabelText,
                              activeColor: Colors.blue,
                              inactiveColor: Colors.white,
                              onChanged: providerArticle.onChangeFontSize),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(child: SimpleText(text: ' فونت :')),
                      ),
                      Expanded(
                        flex: 4,
                        child: Center(
                            child: DropdownButton(
                          dropdownColor: Color(0xFFB1F2B36),
                          value: providerArticle.fontVal,
                          onChanged: providerArticle.updateFontVal,
                          items: List.generate(fontList.length, (index) {
                            return DropdownMenuItem(
                              child: Container(
                                  child: SimpleText(
                                text: fontList[index].name,
                              )),
                              value: fontList[index].val,
                            );
                          }),
                        )),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(child: SimpleText(text: ' رنگ :')),
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(
                          height: 50,
                          child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemCount: myArticlesColor.length,
                            itemBuilder: (context, index) => GestureDetector(
                              onTap: () {
                                providerArticle.onChangeMyArticleColor(
                                    myArticlesColor[index], index);
                              },
                              child: new Container(
                                child: Center(
                                  child: Text(
                                    'ب',
                                    style: TextStyle(
                                        color:
                                            myArticlesColor[index].textColor,
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                width: 50.0,
                                height: 30.0,
                                decoration: new BoxDecoration(
                                  color:
                                      myArticlesColor[index].backgroundColor,
                                  borderRadius: new BorderRadius.all(
                                      new Radius.circular(50.0)),
                                  border: new Border.all(
                                    color:
                                        providerArticle.myArticleColorIndex ==
                                                index
                                            ? Colors.redAccent
                                            : Colors.white,
                                    width: 1.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
