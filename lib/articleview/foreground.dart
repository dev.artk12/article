import 'package:article/colors/font.dart';
import 'package:article/components/articletext.dart';
import 'package:article/providers/articleview.dart';
import 'package:flutter/cupertino.dart';

class ForeGround extends StatelessWidget {
  final ScrollController controller;
  final ProviderArticle providerArticle;
  final String text;
  ForeGround({this.controller, this.text, this.providerArticle});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: controller,
      child: Container(
        margin: const EdgeInsets.only(top: 8, left: 20, right: 8, bottom: 8),
        child: ArticleText(
          size: providerArticle.fontSize,
          color: providerArticle.myArticleColor.textColor,
          text: text,
          font: MyFont.getListDropDown()[providerArticle.fontVal].name,
        ),
      ),
    );
  }
}
